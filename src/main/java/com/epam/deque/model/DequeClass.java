package com.epam.deque.model;

import com.epam.deque.helper.Constants;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class DequeClass<T> implements Iterable<T> {

    private T[] array;
    private int size;
    private int start;
    private int end;

    public DequeClass() {
        array = (T[]) new Object[Constants.MIN];
        size = Constants.MIN;
        start = Constants.INITIAL_START;
        end = Constants.INITIAL_END;
    }

    private class ArrayIterator implements Iterator<T> {

        private int current = Constants.INITIAL_SIZE;

        public boolean hasNext() {
            return current < size;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            T obj = array[(start + Constants.INITIAL_END + current) % array.length];
            current++;
            return obj;
        }
    }

    private boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void addFirst(T obj) {
        if (size == array.length) {
            increaseSize(Constants.MIN * array.length);
        }
        if (start == Constants.INITIAL_SIZE) {
            start = array.length - 1;
        } else {
            array[start--] = obj;
        }
        size++;
    }

    public void addLast(T obj) {
        if (size == array.length) {
            increaseSize(Constants.MIN * array.length);
        }
        if (end == array.length - 1) {
            end = Constants.INITIAL_START;
        } else {
            array[end++] = obj;
        }
        size++;
    }

    public void removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException("Empty queue.");
        }
        if (start == end) {
            start--;
            end--;
        } else if (start == array.length - 1) {
            start = Constants.INITIAL_START;
        } else {
            start++;
        }
        size--;
    }

    public void removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException("Empty queue.");
        }
        if (start == end) {
            start--;
            end--;
        } else if (end == Constants.INITIAL_START) {
            end = array.length - 1;
        } else {
            end--;
        }
        size--;
    }

    private void increaseSize(int size) {
        T[] newQueue = (T[]) new Object[size];
        for (int i = Constants.INITIAL_SIZE; i < this.size; i++) {
            newQueue[i] = array[(start + Constants.INITIAL_END + i) % array.length];
        }
        array = newQueue;
        start = array.length - Constants.INITIAL_END;
        end = this.size;
    }

    public Iterator<T> iterator() {
        return new ArrayIterator();
    }

    public void print() {
        for (T obj : array) {
            System.out.println(obj);
        }
    }
}
