package com.epam.controller;

import com.epam.view.ConsoleView;

public class Main {
    public static void main(String [] args){
        ConsoleView view = new ConsoleView();
        view.show();
    }
}