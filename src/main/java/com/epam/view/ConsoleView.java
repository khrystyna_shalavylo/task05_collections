package com.epam.view;

import com.epam.model.Tree;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView {
    private static Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Tree myTree;

    public ConsoleView() {
        myTree = new Tree();
        menu = new LinkedHashMap<String, String>();
        methodsMenu = new LinkedHashMap<String, Printable>();
        menu.put("1", "1 - Insert the element.");
        menu.put("2", "2 - Remove the element.");
        menu.put("3", "3 - Print the tree.");
        menu.put("4", "4 - Print all keys.");
        menu.put("5", "5 - Print all value.");
        menu.put("6", "6 - Print tree size and check presence of the element.");
       // menu.put("7", "6 - Check presence of the element.");
        menu.put("Q", "Q - Exit");

        methodsMenu.put("1", this::insertElement);
        methodsMenu.put("2", this::removeElement);
        methodsMenu.put("3", this::printTree);
        methodsMenu.put("4", this::printKeys);
        methodsMenu.put("5", this::printValue);
        methodsMenu.put("6", this::printTreeSize);
        methodsMenu.put("7", this::checkPresence);
    }

    private void outputMenu() {
        System.out.println("\nMenu:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    private void insertElement() {
        String value;
        String key;
        System.out.print("\nInput your element: ");
        value = input.nextLine();
        System.out.print("\nInput key of this element: ");
        key = input.nextLine();
        myTree.put(key, value);
    }

    private void removeElement() {
        String key;
        System.out.print("\nInput key of this element: ");
        key = input.nextLine();
        myTree.remove(key);
    }

    private void printTree() {
        System.out.println("Your tree: ");
        myTree.print(myTree.getRoot());
    }

    private void printKeys() {
        System.out.println("Keys of each element:\n" + myTree.keySet());
    }

    private void printValue() {
        System.out.println("Value of each element:\n" + myTree.values());
    }

    private void printTreeSize() {
        System.out.println("Tree size = " + myTree.size());
    }

    private void checkPresence() {
        String key;
        System.out.print("\nInput your element: ");
        key = input.nextLine();
        if (myTree.containsValue(key) == true) {
            System.out.println("\nThe element presences in my tree.");
        } else {
            System.out.println("\nThe element don`t presence in my tree.");
        }
    }


    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("\nPlease, make your choice: ");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
